#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>


void DoCalculation(std::vector<unsigned int>& vector)
{
    vector.at(1) = 12;
    vector.at(2) = 2;

    for (std::size_t i = 0; i < vector.size(); i+=4) {
        if (vector.at(i) == 1) {
            vector.at(vector.at(i+3)) = vector.at(vector.at(i+1)) + vector.at(vector.at(i+2));
        }
        else if (vector.at(i) == 2)
        {
            vector.at(vector.at(i+3)) = vector.at(vector.at(i+1)) * vector.at(vector.at(i+2));
        }
        else if (vector.at(i) == 99)
        {
            break;
        }
    }

    return;
}

int main()
{
    std::string inputFilename{"/home/qxz0abk/advent_of_code/advent_of_code_3/input_3.txt"};
    /*std::cout << "Enter file name: " << std::endl;
    std::cin >> inputFilename;
     */
    std::ifstream inputStream;
    std::string line;
    inputStream.exceptions(std::ifstream::badbit);
    try {
        inputStream.open(inputFilename);
        std::vector<unsigned int> numbers;
        unsigned int number;
        while ((inputStream >> number) && inputStream.ignore())
        {
            numbers.push_back(number);
        }
        DoCalculation(numbers);

        std::cout << numbers.at(0) << std::endl;

    }
    catch (const std::ifstream::failure &e) {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    inputStream.close();
    return 0;
}

